const express = require('express');
const app = express();
const port = 3001;
var bodyParser = require('body-parser')
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/apply-for-jobs-db', { useNewUrlParser: true });

const controllers = require('./controllers/ad');

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/create_ads', (req, res) => controllers.createAds(req, res));

app.get('/get_ads', (req, res) => controllers.getAds(req, res));

app.post('/register_application', (req, res) => controllers.registerApplication(req, res));

app.listen(port, (err) => {
  if (err) {
    return console.log('something bad happened', err)
  }
  console.log(`server is listening on ${port}`)
})