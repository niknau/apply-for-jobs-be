const AdModel = require('../models/Ad');

const adExamples = [
  { title: 'Customer Service Representative @Volvo Group', description: 'We keep on growing. Grow with us! For the expansion of our team in Upplands-Vasby, Sweden we are in search of an engaged and competent person at the earliest possible date for the position of' },
  { title: 'Active Directory specialist to SEB', description: 'Do you want to work with one of the most experienced AD teams in Sweden? Then this it the right job for you! You will be part of a small team with a total AD-experience of almost 60 years. The responsibility comprises operations, maintenance and development of the AD platform and is a part of the Hosting & Integration area, with the aim to provide SEB with world-class hosting services.' },
  { title: 'Breakfast & Café Waiter part time at Hobo', description: 'We are looking for people who are creative, humble and have an optimistic approach to life. Your goal is to give our guests an unforgettably positive experience, and make sure no details goes unnoticed. Humor is the key – we have a lot of fun at work.' },
  { title: 'MARKETING COORDINATOR @ PUMA SWEDEN', description: 'At PUMA, we are in constant pursuit of faster. That extends beyond our support of the fastest athletes in the world. We also work to be fast in how we adapt to and connect with the constantly changing world around us. Through innovative design, iconic footwear and apparel, and authentic partnerships, we aim to always push what’s next in both sport and culture.' },
  { title: 'Street sales associate - In Store Music Sweden @ Epidemic Sound', description: 'The mission involves prospecting, visiting and selling our licensed music service to stores, hotels, restaurants, cafes, gyms, etc. to help the customer improve the retail experience, increase sales, and reduce the existing music costs.' }
]

module.exports.createAds = function (req, res) {
  for (ad in adExamples) {
    const { title, description } = adExamples[ad];
    const newAd = new AdModel({ title, description });
    newAd.save();
  }
  AdModel.find(null, function (err, ads) {
    res.send({ body: JSON.stringify(ads) })
  })
};

module.exports.getAds = function (req, res, next) {
  AdModel.find(null, function (err, ads) {
    res.send({ body: JSON.stringify(ads) })
  })
};

module.exports.registerApplication = function (req, res, next) {
  const { adId, candidate } = req.body;
  AdModel.findOneAndUpdate({ _id: adId }, { $push: { applications: candidate } }, { new: true }, function (err, ad) {
    if (err) {
      res.send(JSON.stringify({ error: true }))
    } else {
      res.send({
        success: true,
        ad
      })
    }
  });
};
