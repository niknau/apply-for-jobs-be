var mongoose = require('mongoose');
const Schema = mongoose.Schema;

let Ad = new Schema({
  title: { type: String, required: true },
  description: { type: String, "default": '' },
  applications: { type: Array, "default": [] }
});

module.exports = mongoose.model('Ad', Ad);