## The server side of apply-for-jobs-SPA

##Getting Started
###MongoDB
1. Download MongoDB and install it. A helpful guide on how to get started with MongoDB: https://docs.mongodb.com/v3.2/tutorial/install-mongodb-on-windows/.

###Run the server
1. Clone this repository
2. "npm install"
3. "npm/yarn start"